module.exports = {
  apps: {
    name: 'GET_CUSTOMER_DETAILS',
    script: '/app/CUSTOMER_DETAILS/GET_CUSTOMER_DETAILS/index.js',
    instances: 1,
    max_memory_restart: '200M',
    error_file: '/applogs/CUSTOMER_DETAILS/GET_CUSTOMER_DETAILS/PM2/err.log',
    out_file: '/applogs/CUSTOMER_DETAILS/GET_CUSTOMER_DETAILS/PM2/out.log',
    log_file: '/applogs/CUSTOMER_DETAILS/GET_CUSTOMER_DETAILS/PM2/combined.log',
    exec_mode: 'cluster',
  },
}
