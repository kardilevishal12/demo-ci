# Use a Node.js base image from Quay.io (equivalent to Docker Hub)
FROM node:17-slim
# Set the working directory inside the container
WORKDIR /app

COPY . /app
# Install application dependencies
RUN npm install --quiet --no-progress


# Expose the port that the app runs on
EXPOSE 5003

# Define the command to start the application
ENTRYPOINT ["npm", "start"]
