const GET_USER_DETAILS = `SELECT * FROM Users where id = @id`

const GET_USER_PG_DETAILS = `SELECT * FROM students where id = $1`

const mssqlSp = `EXEC SelectUser @Id = @Id`

const postgresSp = `select * from GetStudentById($1);`

const postgresInsertSp = `CALL AddStudents($1, $2, $3, $4);`

const insertUser = `INSERT INTO [Test].[dbo].[Users]
  (id, name, isDeleted)
  OUTPUT inserted.id
  VALUES
  (@id, @name, @isDeleted)`

module.exports = {
  GET_USER_DETAILS,
  GET_USER_PG_DETAILS,
  mssqlSp,
  postgresSp,
  postgresInsertSp,
  insertUser
}
