const sql = require('mssql')
const config = require('../config/config')
require('bytenode')
const df = require('./decryption/hg')

if (config.db_mssql_test.enable == 'false') {
  return
}

const dbConfig = {
  user: config.db_mssql_test.username,
  password: config.db_mssql_test.password,
  server: config.db_mssql_test.host,
  database: config.db_mssql_test.name,
  port: Number(config.db_mssql_test.port),
  schema: config.db_mssql_test.schema,
  options: {
    encrypt: false,
    enableArithAbort: true,
  },
}

const pool = new sql.ConnectionPool(dbConfig)

const poolConnect = pool
  // .connect()
  // .then(() => {
  //   console.log(
  //     'Successfully Connected to Database : ' +
  //       dbConfig.server +
  //       '/' +
  //       dbConfig.database
  //   )
  // })
  // .catch((err) => {
  //   console.error('Error Connecting to Database-: MSSQL \n', err)
  // })

module.exports = {
  pool,
  poolConnect,
}
