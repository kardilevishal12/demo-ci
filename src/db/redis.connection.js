// const redis = require('redis')
// const { unzipData, zipData } = require('../utils/encryption')
// const { logExceptions } = require('../shared/logger')

// const redisConfig = {
//   host: global.gConfig.db_redis.host,
//   port: global.gConfig.db_redis.port,
//   pass: '',
//   tls: { checkServerIdentity: () => undefined },
// }

// const client =
//   process.env.db_redis_enable === 'true' ? redis.createClient() : {} //redisConfig

// if (process.env.db_redis_enable === 'true') {
//   // client.on('error', (error) => {
//   //   console.log('Error in redis connection')
//   //   logExceptions('', 'Redis Connection', error)
//   // })
// }

// if (process.env.db_redis_enable === 'true') {
//   // client.on('t', () => {
//   //   if (process.env.db_redis_enable === 'false') {
//   //     return
//   //   }
//   //   console.error('Successfully Connected to Redis') //  + redisConfig.host
//   // })
// }

// const setDataRedis = async (key, value) => {
//   if (process.env.db_redis_enable === 'false') {
//     return
//   }

//   client.set(key, value, (err) => {
//     if (err) {
//       return false
//     }
//   })
//   console.log('Set Redis Called :::: 2')
//   return true
// }

// const setDataEXRedis = async (key, ttl, value) => {
//   if (process.env.db_redis_enable === 'false') {
//     return
//   }

//   // 24 hrs => 24 * 60 * 60
//   // 1 hr => 60 * 60
//   client.setex(key, ttl, value, (err) => {
//     if (err) {
//       return false
//     }
//   })
//   console.log('Set Redis EX Called :::: 3')
//   return true
// }

// const getDataRedis = async (key) => {
//   if (process.env.db_redis_enable === 'false') {
//     return
//   }

//   return new Promise((resolve, reject) => {
//     client.get(key, async (err, data) => {
//       if (err) {
//         reject(err)
//       }
//       console.log('Get Redis Called :::: 1')
//       resolve(data)
//     })
//   })
// }

// const deleteRedisData = async (key) => {
//   if (process.env.db_redis_enable === 'false') {
//     return
//   }

//   return new Promise((resolve, reject) => {
//     client.del(key, function (err, response) {
//       if (err) {
//         reject(err)
//       }
//       resolve(response)
//     })
//   })
// }

// const deleteMultpleRedisData = async (key1) => {
//   if (process.env.db_redis_enable === 'false') {
//     return
//   }

//   return new Promise((resolve, reject) => {
//     client.keys(key1, (err, keys) => {
//       if (keys && keys.length) {
//         keys.forEach((key) => {
//           client.del(key, (err, response) => {
//             if (err) {
//               reject(err)
//             }
//             resolve({ count: keys.length, response })
//           })
//         })
//       } else {
//         resolve({ count: 0 })
//       }
//     })
//   })
// }

// const redisUtility = async (type, ttl, key, value = {}) => {
//   if (type === 'GET') {
//     const isKeyExists = await getDataRedis(key)
//     if (global.gConfig.db_redis.encryption === 'true' && isKeyExists) {
//       return await unzipData(isKeyExists)
//     }
//     if (global.gConfig.db_redis.encryption === 'false' && isKeyExists) {
//       return isKeyExists
//     }
//     return null
//   }

//   if (type === 'SET') {
//     let setData = value
//     if (typeof setData === 'object') setData = JSON.stringify(setData)
//     if (global.gConfig.db_redis.encryption === 'true') {
//       return await setDataRedis(key, await zipData(setData))
//     }
//     if (global.gConfig.db_redis.encryption === 'false') {
//       return await setDataRedis(key, setData)
//     }
//     return null
//   }

//   if (type === 'SET_EX') {
//     let setData = value
//     if (typeof setData === 'object') setData = JSON.stringify(setData)
//     if (global.gConfig.db_redis.encryption === 'true') {
//       return await setDataEXRedis(key, ttl, await zipData(setData))
//     }
//     if (global.gConfig.db_redis.encryption === 'false') {
//       return await setDataEXRedis(key, ttl, setData)
//     }
//     return null
//   }

//   if (type === 'DELETE') {
//     return await deleteRedisData(key)
//   }

//   if (type === 'DELETE_MULTI') {
//     return await deleteMultpleRedisData(key)
//   }

//   return null
// }

// const publishRedis = async (channel, message) => {
//   await client.publish(channel, message);
//   console.log('Publish Redis Called')
// }

// const subscribeRedis = async (channel, callback) => {
//   const clientDup = process.env.db_redis_enable === 'true' ? redis.createClient() : {}
//   await clientDup.subscribe(channel);
//   clientDup.on("message", async (ch, message) => {
//     if (channel === ch) {
//       await callback(message);
//     }
//   });
//   console.log('Subscribe Redis Called')
// }

// module.exports = {
//   redisUtility,
//   publishRedis,
//   subscribeRedis,
// }
