const Pool = require('pg').Pool;
const config = require('../config/config')

const dbConfig = {
    user: config.db_postgres_students.username,
    password: config.db_postgres_students.password,
    host: config.db_postgres_students.host,
    database: config.db_postgres_students.name,
    port: Number(config.db_postgres_students.port),
    schema: config.db_postgres_students.schema,
    // options: {
    //   encrypt: false,
    //   enableArithAbort: true,
    // },
}

const pool = new Pool(dbConfig)

const poolConnect = pool
  // .connect()
  // .then(() => {
  //   console.log(
  //     'Successfully Connected to Database : ' +
  //       dbConfig.host +
  //       '/' +
  //       dbConfig.database
  //   )
  // })
  // .catch((err) => {
  //   console.error('Error Connecting to Database-: Postgres \n', err)
  // })

module.exports = {
    pool,
    poolConnect,
}