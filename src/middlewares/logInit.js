const { toDatetime, v1, logs } = require('../modules/filelogger')

var addr
require('dns').lookup(require('os').hostname(), function (err, add) {
  addr = add
})
module.exports = function (req, res, next) {
  const id = v1()

  logs.info('Request ---> ' + id + ' ---> Started')
  const ServiceType = Object.keys(req.body)[0]
  res.filelog = {
    AUDITLOG_REQUEST_ID: req.body[`${ServiceType}`].RequestHeader.MessageKey.RequestUUID || id,
    SERVICE_NAME: ServiceType,
    REQUESTUUID:
      req.body[`${ServiceType}`].RequestHeader.MessageKey.RequestUUID || '',
    MSGFLOWNAME: ServiceType,
    CHANNELID:
      req.body[`${ServiceType}`].RequestHeader.MessageKey.ChannelId || '',
    TIMEZONE: 'GMT 5:30',
    RQ_MESSAGEDATETIME: toDatetime(),
    CUSTOMERID: '',
    ACCNO: '',
    CARDNO: '',
    CREATEDBY: ServiceType,
    CREATEDDATE: toDatetime(),
    RQ_PAYLOAD_TP: '',
    RQ_PAYLOAD: '',
    SERREQID:
      req.body[`${ServiceType}`].RequestHeader.MessageKey.ServiceRequestId ||
      '',
    SESSIONID: '',
    SERVERIP: addr,
    RS_MESSAGEDATETIME: '',
    ERRORCODE: '',
    ERRORDESC: '',
    ERRORSOURCE: '',
    ERRORTYPE: '',
    RS_PAYLOAD_TP: '',
    RS_PAYLOAD: '',
    RESPONSE_CODE: '',
    RESPONSE_STATUS: '',
  }

  if (req.body) {
    res.filelog.RQ_PAYLOAD = JSON.stringify(req.body)
  }
  next()
}
