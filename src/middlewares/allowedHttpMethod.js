const allowedHttpMethods = (req, res, next) => {
  const method = req.method
  const allowedMethods = ['POST', 'GET']
  let whiteListAPI = true
  if (
    ('dev' !== 'prod' && req.url.match('api-docs')) ||
    req.url.match('healthCheck')
  ) {
    whiteListAPI = false
  }

  if (whiteListAPI) {
    if (!allowedMethods.includes(method)) {
      return res.status(405).send({
        checkappupdate: {
          StatusCode: 1,
          ErrorCode: '405',
          ErrorMessage: 'Method Not Allowed',
          DisplayText: 'Method Not Allowed',
        },
      })
    }
  }

  next()
}
module.exports = allowedHttpMethods
