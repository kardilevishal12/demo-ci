const responseHandler = require('../shared/responseHandler')
const errorCodes = require('../services/restApi/constants/genErrorCode.json')
const logger = require('../shared/logger')

const setRequestTimeout = (req, res, next) => {
  res.setTimeout(Number(global.gConfig.timeout), function () {
    logger.logExceptions('', 'setRequestTimeout', errorCodes['408'].errorMsg)
    let serviceRequestId = Object.keys(req.body)[0]
    let response = responseHandler.constructErrorResponse({
      ServiceRequestId: serviceRequestId,
      ResponseHeader: req.body[`${serviceRequestId}`].RequestHeader,
      DisplayText: errorCodes['408'].errorMsg,
      ErrorCode: errorCodes['408'].errorCode,
      ErrorMessage: errorCodes['408'].errorMsg,
    })
    return res.status(408).send(response)
  })

  return next()
}

module.exports = setRequestTimeout
