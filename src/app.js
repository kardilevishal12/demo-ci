const express = require('express')
const cors = require('cors')
const config = require('./config/config')
const morgan = require('./config/morgan')
const reqTimeout = require('./middlewares/requestTimeout')
const allowedHttpMethods = require('./middlewares/allowedHttpMethod')
const malformedRequest = require('./middlewares/malformedRequest')
const notFound = require('./middlewares/notFound')
// const { subscribeRedis } = require('../src/db/redis.connection')

const app = express()
app.disable('x-powered-by')
if (config.env !== 'prod') {
  app.use(morgan.successHandler)
  app.use(morgan.errorHandler)
}

require('./db/mssqlDB.connection')
require('./db/postgresDB.connection')
// require('./db/redis.connection')
require('./services/jobs')

const corsOptions = {
  origin: ['*.rblbank.com', '*.ratnakarbank.in'],
  optionsSuccessStatus: 200,
}
app.use(cors(corsOptions))

app.use(malformedRequest)
app.use(allowedHttpMethods)
app.use(reqTimeout)

app.use('/getUsers', require('./services/restApi/routes/v1'))

app.use(notFound)

process.on('uncaughtException', (err) => {
  console.error('uncaughtException', err)
})

process.on('unhandledRejection', (err) => {
  console.error('unhandledRejection', err)
})

// This method should be implemented in another peer.
// subscribeRedis("testChannel", (message) => {
//   console.log("Received message:", JSON.parse(message));
// });

app.listen(global.gConfig.port, () => {
  console.table([
    {
      PORT: Number(global.gConfig.port),
      ENV: process.env.NODE_ENV.toUpperCase(),
    },
  ])
})
