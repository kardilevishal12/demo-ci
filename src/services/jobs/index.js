// const moment = require('moment-timezone')
// const cron = require('node-cron')
// // const { redisUtility } = require('../../db/redis.connection')

// // Per Min Testing */1 * * * *
// // Every 3 HRS 0 */3 * * *
// // Schedule 0 0 * * *

// /**
//  * @description Schedular time reference link https://crontab.guru/#0_0_*_*_*
//  *  # ┌────────────── second (optional)
//  # │ ┌──────────── minute
//  # │ │ ┌────────── hour
//  # │ │ │ ┌──────── day of month
//  # │ │ │ │ ┌────── month
//  # │ │ │ │ │ ┌──── day of week
//  # │ │ │ │ │ │
//  # │ │ │ │ │ │
//  # * * * * * *
//  */

// /**
//  * @description This cron will flush all the records every midnight at 12:00 O'clock.
//  */
// global.gConfig.db_redis.restart_required === 'YES' &&
//   Promise.allSettled([
//     redisUtility('DELETE_MULTI', null, `USER*`),
//   ]).then((results) => {
//     console.log(
//       'Redis Data Flushed ::: ',
//       moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
//     )
//     console.log('-------------------------------------------')
//     console.table(results)
//     console.log('-------------------------------------------')
//   })

// // cron.schedule(
// //   '0 0 * * *',
// //   async () => {
// //     console.log('-------------------------------------------')
// //     console.log(
// //       'Schedular Started @ ',
// //       moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
// //     )
// //     const userDetails = await redisUtility(
// //       'DELETE_MULTI',
// //       null,
// //       `USER*`
// //     )
    
// //     console.table([
// //       userDetails,
// //     ])
// //     console.log(
// //       'Schedular Stop @ ',
// //       moment(new Date()).format('YYYY-MM-DD HH:mm:ss')
// //     )
// //     console.log('-------------------------------------------')
// //   },
// //   {
// //     scheduled: true,
// //     timezone: 'Asia/Kolkata',
// //   }
// // )
