const Joi = require('joi')
const responseHandler = require('../../../shared/responseHandler')
const errorCodes = require('../constants/genErrorCode.json')

const isDataValid = (ReqBody) => {
  const schema = Joi.object()
    .keys({
      getUser: Joi.object()
        .keys({
          RequestBody: Joi.object()
            .keys({
              getUserRequestBody: Joi.object().keys({
                Id: Joi.string().required(),
              }),
            })
            .required(),
          RequestHeader: validateRequestHeader(),
        })
        .required(),
    })
    .required()

  const isError = schema.validate(ReqBody)
  if (isError.error && isError.error.details && isError.error.details.length) {
    const serviceType = Object.keys(ReqBody)[0]
    const errorMsg = isError.error.details[0].message
    const err = responseHandler.constructErrorResponse({
      ServiceRequestId: serviceType,
      ResponseHeader: ReqBody[`${serviceType}`]
        ? ReqBody[`${serviceType}`].RequestHeader
        : {},
      DisplayText: errorCodes['010'].errorMsg,
      ErrorCode: errorCodes['010'].errorCode,
      ErrorMessage: errorMsg || errorCodes['010'].errorMsg,
    })
    return { error: true, data: err }
  }

  return { error: false, data: null }
}

const isHeadersValid = (ReqHeaders, ReqBody) => {
  const schema = Joi.object({
    "x-api-key": Joi.string().required(),
  })

  const isError = schema.validate(ReqHeaders)
  if (isError.error && isError.error.details && isError.error.details.length) {
    const serviceType = Object.keys(ReqBody)[0]
    const errorMsg = isError.error.details[0].message
    const err = responseHandler.constructErrorResponse({
      ServiceRequestId: serviceType,
      ResponseHeader: ReqBody[`${serviceType}`]
        ? ReqBody[`${serviceType}`].RequestHeader
        : {},
      DisplayText: errorCodes['010'].errorMsg,
      ErrorCode: errorCodes['010'].errorCode,
      ErrorMessage: errorMsg || errorCodes['010'].errorMsg,
    })

    return { error: true, data: err }
  }

  return { error: false, data: null }
}

const validateRequestHeader = () => {
  return Joi.object()
    .keys({
      AdditionalInfo: Joi.object()
        .keys({
          JourneyId: Joi.string().max(20).allow(null).allow('').required(),
          LanguageId: Joi.string().max(20).allow(null).allow('').required(),
          SVersion: Joi.string().max(20).allow(null).allow('').required(),
          SessionId: Joi.string().max(1000).allow(null).allow('').required(),
        })
        .required(),
      RequestMessageInfo: Joi.object()
        .keys({
          BankId: Joi.string().max(20).allow(null).allow('').required(),
          TimeZone: Joi.string().max(50).allow(null).allow('').required(),
          MessageDateTime: Joi.string()
            .max(50)
            .allow(null)
            .allow('')
            .required(),
        })
        .required(),
      MessageKey: Joi.object()
        .keys({
          RequestUUID: Joi.string().max(50).required(),
          ServiceRequestId: Joi.string().max(50).required(),
          ServiceRequestVersion: Joi.string().max(10).required(),
          ChannelId: Joi.string().max(50).required(),
        })
        .required(),
      DeviceInfo: Joi.object()
        .keys({
          DeviceFamily: Joi.string().max(100).allow(null).allow('').required(),
          DeviceFormat: Joi.string().max(100).allow(null).allow('').required(),
          DeviceType: Joi.string().max(100).allow(null).allow('').required(),
          DeviceName: Joi.string().max(100).allow(null).allow('').required(),
          DeviceIMEI: Joi.string().max(100).allow(null).allow('').required(),
          DeviceID: Joi.string().max(100).allow(null).allow('').required(),
          DeviceVersion: Joi.string().max(100).allow(null).allow('').required(),
          AppVersion: Joi.string().max(100).allow(null).allow('').required(),
          DeviceOS: Joi.string().max(100).allow(null).allow('').required(),
          DeviceIp: Joi.string().max(100).allow(null).allow('').required(),
        })
        .required(),
      Security: Joi.object()
        .keys({
          Token: Joi.object()
            .keys({
              Certificate: Joi.string()
                .max(1000)
                .allow(null)
                .allow('')
                .required(),
              MessageHashKey: Joi.string()
                .max(500)
                .allow(null)
                .allow('')
                .required(),
              MessageIndex: Joi.string()
                .max(100)
                .allow(null)
                .allow('')
                .required(),
              PasswordToken: Joi.object()
                .keys({
                  UserId: Joi.string()
                    .max(100)
                    .allow(null)
                    .allow('')
                    .required(),
                  Password: Joi.string()
                    .max(100)
                    .allow(null)
                    .allow('')
                    .required(),
                })
                .allow(null)
                .required(),
            })
            .required(),
        })
        .required(),
    })
    .required()
}

module.exports = {
  isDataValid,
  isHeadersValid,
}
