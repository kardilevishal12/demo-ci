const { logExceptions } = require('../../../shared/logger')
const errorCodes = require('../constants/genErrorCode.json')
const responseHandler = require('../../../shared/responseHandler')
const { USER_DETAILS, USER_DETAILS_PG, USER_DETAILS_SP, USER_DETAILS_PG_SP, INSERT_USER_DETAILS_PG_SP, SAVE_USER_DETAILS } = require('../subscribers/DBOperations.subsribers')
const { redisUtility } = require('../../../db/redis.connection')
const {ElasticCache, sendMessageToRedis} = require('../subscribers/ElasticCache.subscibers')


const getUser = async (reqBody, requestUUID) => {
  const serviceRequestId = Object.keys(reqBody)[0]

  try {
    const reqParams = reqBody[`${serviceRequestId}`].RequestBody[`${serviceRequestId}RequestBody`]

    // Cache-aside pattern
    let userData
    const ElasticCacheData = await ElasticCache(
      `USER_${reqParams.Id}`
    )
    
    if (!ElasticCacheData?.exists) {
      const usersData = await USER_DETAILS(reqParams)
      if (usersData.error) {
        return responseHandler.constructErrorResponse({
          ServiceRequestId: serviceRequestId,
          ResponseHeader: reqBody[`${serviceRequestId}`].RequestHeader,
          DisplayText: usersData.data,
          ErrorCode: errorCodes['999'].errorCode,
          ErrorMessage: errorCodes['999'].errorMsg,
        })
      }
      userData = usersData
      await redisUtility('SET', null, `USER_${reqParams.Id}`, usersData)
    }
    else {
      userData = ElasticCacheData
    }

    // Redis publish
    // if(userData.data) {
    //   await sendMessageToRedis(JSON.stringify(userData))
    // }
    
    // Write through patterns
    const user = {id: '13', name: 'xyz', isDeleted: 'N'}
    const saveUserData = await SAVE_USER_DETAILS(user)
    if(saveUserData.error) {
      return responseHandler.constructErrorResponse({
        ServiceRequestId: serviceRequestId,
        ResponseHeader: reqBody[`${serviceRequestId}`].RequestHeader,
        DisplayText: saveUserData.error,
        ErrorCode: errorCodes['999'].errorCode,
        ErrorMessage: errorCodes['999'].errorMsg,
      })
    } else {
      await redisUtility('SET', null, `TEST_${user.id}`, user)
    }

    // Test write through 
    const ElasticCacheTestData = await ElasticCache(`TEST_${reqParams.Id}`)
    console.log(ElasticCacheTestData, '-> ElasticCacheTestData')





    const usersPgData = await USER_DETAILS_PG(reqParams)
    if (usersPgData.error) {
      return responseHandler.constructErrorResponse({
        ServiceRequestId: serviceRequestId,
        ResponseHeader: reqBody[`${serviceRequestId}`].RequestHeader,
        DisplayText: usersPgData.error,
        ErrorCode: errorCodes['999'].errorCode,
        ErrorMessage: errorCodes['999'].errorMsg,
      })
    }
    // else {
    //   await redisUtility('READ_THROUGH', null, `READ_${reqParams.Id}`, usersPgData)
    // }

    // // Read through
    // const readThroughTestData = await ElasticCache(`READ_${reqParams.Id}`)
    // console.log(readThroughTestData, '-> readThroughTestData')
    
    const usersSpData = await USER_DETAILS_SP(reqParams)
    if (usersSpData.error) {
      return responseHandler.constructErrorResponse({
        ServiceRequestId: serviceRequestId,
        ResponseHeader: reqBody[`${serviceRequestId}`].RequestHeader,
        DisplayText: usersPgData.error,
        ErrorCode: errorCodes['999'].errorCode,
        ErrorMessage: errorCodes['999'].errorMsg,
      })
    }

    const usersSpPgData = await USER_DETAILS_PG_SP(reqParams)
    if (usersSpPgData.error) {
      return responseHandler.constructErrorResponse({
        ServiceRequestId: serviceRequestId,
        ResponseHeader: reqBody[`${serviceRequestId}`].RequestHeader,
        DisplayText: usersPgData.error,
        ErrorCode: errorCodes['999'].errorCode,
        ErrorMessage: errorCodes['999'].errorMsg,
      })
    }

    // const insertUsersSpPgData = await INSERT_USER_DETAILS_PG_SP(reqParams)
    // if (insertUsersSpPgData.error) {
    //   return responseHandler.constructErrorResponse({
    //     ServiceRequestId: serviceRequestId,
    //     ResponseHeader: reqBody[`${serviceRequestId}`].RequestHeader,
    //     DisplayText: usersPgData.error,
    //     ErrorCode: errorCodes['999'].errorCode,
    //     ErrorMessage: errorCodes['999'].errorMsg,
    //   })
    // }
    

    return responseHandler.constructSuccessResponse({
      ServiceRequestId: serviceRequestId,
      ResponseHeader: reqBody[`${serviceRequestId}`].RequestHeader,
      ResponseBody: { getUserResponseBody : true },
      DisplayText: errorCodes['000'].errorMsg,
      ErrorCode: errorCodes['000'].errorCode,
      ErrorMessage: errorCodes['000'].errorMsg,
    })
  } catch (err) {
    logExceptions(requestUUID, 'getUser', err)
    return responseHandler.constructErrorResponse({
      ServiceRequestId: serviceRequestId,
      ResponseHeader: reqBody[`${serviceRequestId}`].RequestHeader,
      DisplayText: errorCodes['008'].errorMsg,
      ErrorCode: errorCodes['008'].errorCode,
      ErrorMessage: errorCodes['008'].errorCode,
    })
  }
}
module.exports = {
  getUser,
}
