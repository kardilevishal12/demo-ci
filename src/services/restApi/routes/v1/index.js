const express = require('express')
const ShowPopupRoute = require('./user.router')
const HealthCheck = require('./HealthCheck')
const swaggerUi = require('swagger-ui-express')
const contentType = require('../../../../middlewares/contentType')
const logInit = require('../../../../middlewares/logInit')
const swaggerDocument = require('../../swagger/swagger.json')

const router = express.Router()

if (['dev', 'uat'].includes('dev')) {
  const routes = [
    {
      path: '/api/v1',
      route: HealthCheck,
      isHealthCheck: true,
    },
    // {
    //   path: '/api/v1/api-docs/getUser',
    //   isSwagger: true,
    //   requestInterceptor: (request)=>{
    //     //logic goes here...get executed before your request is made
    //     request.headers['x-api-key'] = request.headers['x-api-key'];
    //   }
    // },
  ]

  routes.forEach((route) => {
    if (route.isSwagger) {
      router.use(
        route.path,
        swaggerUi.serve,
        swaggerUi.setup(swaggerDocument, {
          swaggerOptions: {
            defaultModelsExpandDepth: -1,
          },
        }),
        route.requestInterceptor
      )
    } else {
      router.use(route.path, route.route)
    }
  })
}

const defaultRoutes = [
  {
    path: '/api/v1',
    route: ShowPopupRoute,
  },
]

router.use(contentType)
router.use(logInit)

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route)
})

module.exports = router
