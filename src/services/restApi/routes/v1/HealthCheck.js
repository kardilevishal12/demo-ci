const express = require('express')
const router = express.Router()

router.get('/healthCheck', async (req, res) => {
  res.status(200).send({
    checkappupdate: {
      StatusCode: 0,
      ErrorCode: '200',
      ErrorMessage: 'Healthcheck API is Working.',
      DisplayText: 'Healthcheck API is Working.',
    },
  })
})

module.exports = router
