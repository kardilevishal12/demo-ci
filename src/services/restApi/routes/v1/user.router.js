const express = require('express')
const router = express.Router()
const fileLogger = require('../../../../middlewares/fileLogger')
const validation = require('../../validations/user.validation')

const { getUser } = require('../../controllers/userController')

router.post('/user', async (req, res, next) => {
  res.body = {}
  const errResponse = await validation.isDataValid(req.body)
  if (errResponse.error) {
    res.filelog.RESPONSE_CODE = 1
    res.filelog.RESPONSE_STATUS = 'FAILURE'
    res.body = errResponse.data
    res.status(200).send(errResponse.data)
    return next()
  }
  
  let headers = {
    'x-api-key': req.headers['x-api-key']
  }
  const errHeaderResponse = await validation.isHeadersValid(headers, req.body)
  if (errHeaderResponse.error) {
    res.filelog.RESPONSE_CODE = 1
    res.filelog.RESPONSE_STATUS = 'FAILURE'
    res.body = errHeaderResponse.data
    res.status(200).send(errHeaderResponse.data)
    return next()
  }

  const response = await getUser(
    req.body,
    res.filelog.AUDITLOG_REQUEST_ID
  )

  if (response.getUser.Status.StatusCode != '0') {
    res.filelog.RESPONSE_CODE = 1
    res.filelog.RESPONSE_STATUS = 'FAILURE'
  }
  res.body = response
  res.status(200).send(response)

  return next()
})

router.use(fileLogger)

module.exports = router
