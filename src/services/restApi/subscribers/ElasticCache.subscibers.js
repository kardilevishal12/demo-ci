const { redisUtility, publishRedis } = require('../../../db/redis.connection')

const ElasticCache = async (Key) => {
  const keyExist = await redisUtility('GET', null, Key)
  if (keyExist) {
    try {
      const nDetails = JSON.parse(keyExist)
      return { exists: true, data: nDetails }
    } catch (err) {
      return { exists: false }
    }
  }

  return { exists: false }
}

const sendMessageToRedis = async (data) => {
  try {
    console.log(data, ' -> Data')
    await publishRedis("testChannel", data);
    console.log('Message sent successfully!')

  } catch (error) {
    console.log(error);
  }
};


module.exports = {
  ElasticCache,
  sendMessageToRedis,
}
