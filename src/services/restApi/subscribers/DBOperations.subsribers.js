const moment = require('moment')
const sql = require('mssql')
const sqlConnector = require('../../../db/mssqlDB.connection')
const {pool} = require('../../../db/postgresDB.connection')
const {
  GET_USER_DETAILS,
  GET_USER_PG_DETAILS,
  mssqlSp,
  postgresSp,
  postgresInsertSp,
  insertUser
} = require('../../../db/queries/index')
const { logExceptions } = require('../../../shared/logger')

const USER_DETAILS = async (params) => {
  try {
    let request = sqlConnector.pool.request()
    request.input('id', sql.VarChar, params.Id)

    const getUserResult = await request.query(GET_USER_DETAILS)
    if (getUserResult && getUserResult.recordset.length > 0) {
      return { error: false, data: getUserResult.recordset[0], code: '000' }
    }
    else return { error: true, data: 'Data not found', code: '102' }
  } catch (error) {
    return { error: true, data: error, code: '102' }
  }
}

const USER_DETAILS_PG = async (params) => {
  try {
    const getUserResult = await pool.query(GET_USER_PG_DETAILS, [params.Id])
    if (getUserResult && getUserResult.rows.length > 0) {
      return { error: false, data: getUserResult.rows[0], code: '000' }
    }

  } catch (error) {
    return { error: true, data: null, code: '102' }
  }
}

const USER_DETAILS_SP = async (params) => {
  try {
    let request = sqlConnector.pool.request()
    request.input('Id', sql.VarChar, params.Id)

    const getUserResult = await request.query(mssqlSp)
    if (getUserResult && getUserResult.recordset.length > 0) {
      return { error: false, data: getUserResult.recordset[0], code: '000' }
    }

  } catch (error) {
    return { error: true, data: null, code: '102' }
  }
}

const USER_DETAILS_PG_SP = async (params) => {
  try {
    const getUserResult = await pool.query(postgresSp, [params.Id])
    if (getUserResult && getUserResult.rows.length > 0) {
      return { error: false, data: getUserResult.rows[0], code: '000' }
    }

  } catch (error) {
    return { error: true, data: null, code: '102' }
  }
}

const INSERT_USER_DETAILS_PG_SP = async (params) => {
  try {
    const getUserResult = await pool.query(postgresInsertSp, ['Vignesh', 'vignesh@gmail.com', 23, '2000-05-03'])

  } catch (error) {
    console.log(error)
    return { error: true, data: null, code: '102' }
  }
}

const SAVE_USER_DETAILS = async (params) => {
  try {
    let request = sqlConnector.pool.request()

    request.input('id', sql.VarChar, params.id)
    request.input('name', sql.VarChar, params.name)
    request.input('isDeleted', sql.VarChar, params.isDeleted)

    const saveUserResult = await request.query(insertUser)

    if (saveUserResult.rowsAffected > 0 && saveUserResult.recordset[0].id) {
      return { error: false, data: {Status: 'Data saved successfully'}, code: '000' }
    } else {
      return { error: true, data: null, code: '103' }
    }

  } catch (error) {
    return { error: true, data: null, code: '102' }
  }
}

module.exports = {
  USER_DETAILS,
  USER_DETAILS_PG,
  USER_DETAILS_SP,
  USER_DETAILS_PG_SP,
  INSERT_USER_DETAILS_PG_SP,
  SAVE_USER_DETAILS
}
