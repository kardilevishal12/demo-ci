require('winston-daily-rotate-file')
const winston = require('winston')

const { createLogger, format, transports } = winston

function encodeBase64(data) {
  try {
    data = JSON.stringify({ details: data })
    return new Buffer.from(data).toString('base64')
  } catch (error) {
    return error.message
  }
}

const generalLogFormat = format.printf(({ message, timestamp }) => {
  const tag = message.tag || ''
  const data = encodeBase64(message.data)
  const requestUUID = message.requestUUID
  let finalLog = `|**|${
    timestamp + '|~|' + requestUUID + '|~|' + tag + '|~|' + data
  }`
  return finalLog
})

const auditlogFormat = format.printf(({ message, timestamp }) => {
  const requestUUID = message.requestUUID || ''
  const funName = message.data.funName || ''
  const state = message.data.state || 'SUCCESS'
  const reqBody = message.data.req || { message: 'Empty Request Body' }
  const resBody = message.data.res || { message: 'Empty Response Body' }
  const request =
    Buffer.from(JSON.stringify(reqBody)).toString('base64') ||
    'Empty Request Body'
  const response =
    Buffer.from(JSON.stringify(resBody)).toString('base64') ||
    'Empty Response Body'

  let finalLog = `|**|
  ${
    timestamp +
    '|~|' +
    requestUUID +
    '|~|' +
    funName +
    '|~|' +
    request +
    '|~|' +
    state +
    '|~|' +
    response
  }`
  return finalLog
})

const exceptionFormat = format.printf(({ message, timestamp }) => {
  const requestUUID = message.requestUUID || 'RequestUUID'
  const funName = message.functionName || ''
  let data = ''
  if (message.data instanceof Error) {
    data = encodeBase64(message.data.message + ' ' + message.data.stack)
  } else {
    data = encodeBase64(message.data)
  }

  let finalLog = `|**|${
    timestamp + '|~|' + requestUUID + '|~|' + funName + '|~|' + data
  }`
  return finalLog
})

const generalLog = createLogger({
  transports: [
    new transports.DailyRotateFile({
      filename: global.gConfig.logsDirectory + '/general/%DATE%.log',
      zippedArchive: true,
      maxSize: '1024m',
      maxFiles: '3650d',
      format: format.combine(
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
        generalLogFormat
      ),
    }),
  ],
})

const exceptionLogs = createLogger({
  transports: [
    new transports.DailyRotateFile({
      filename: global.gConfig.logsDirectory + '/exceptions/%DATE%.log',
      zippedArchive: true,
      maxSize: '1024m',
      maxFiles: '3650d',
      format: format.combine(
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
        exceptionFormat
      ),
    }),
  ],
})

const auditLogs = createLogger({
  transports: [
    new transports.DailyRotateFile({
      filename: global.gConfig.logsDirectory + '/audit/%DATE%.log',
      zippedArchive: true,
      maxSize: '1024m',
      maxFiles: '3650d',
      format: format.combine(
        format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
        auditlogFormat
      ),
    }),
  ],
})

/**
 * This is used to Log data on console or file
 * @param {*} tag
 * @param {*} data
 * @param {*} UUID
 */
function logGeneralData(requestUUID, tag, data) {
  try {
    //log data in file
    if (global.gConfig.log.file.general_enable == 'true') {
      generalLog.info({ requestUUID, tag, data })
    }
    if (global.gConfig.log.console.general_enable == 'true') {
      console.log(requestUUID, tag, data)
    }
  } catch (error) {
    console.error('Error in logger.js', error)
  }
}

/**
 * Log exception data
 * @param {*} requestUUID
 * @param {*} functionName
 * @param {*} data
 */
function logExceptions(requestUUID, functionName, data) {
  try {
    if (global.gConfig.log.file.exception_enable == 'true') {
      exceptionLogs.error({ requestUUID, functionName, data })
    }
    if (global.gConfig.log.console.exception_enable == 'true') {
      console.log(requestUUID, functionName, data)
    }
  } catch (error) {
    console.error('Error in logger.js', error)
  }
}

/**
 * Log third party request response
 * @param {*} requestUUID
 * @param {*} data
 */
function logAudits(requestUUID, data) {
  try {
    if (global.gConfig.log.file.audit_enable == 'true') {
      auditLogs.info({ requestUUID, data })
    }
    if (global.gConfig.log.console.audit_enable == 'true') {
      //  console.log('API Req Res', requestUUID, data)
    }
  } catch (error) {
    console.error('Error in logger.js', error)
  }
}

module.exports = { logGeneralData, logExceptions, logAudits }
