const constructErrorResponse = (params) => {
  const errorResponse = {}
  errorResponse[params.ServiceRequestId] = {
    ResponseHeader: params.ResponseHeader || {},
    Status: {
      DisplayText: params.DisplayText || '',
      ErrorCode: params.ErrorCode,
      ErrorMessage: params.ErrorMessage || '',
      StatusCode: '1',
    },
  }
  return errorResponse
}

const constructSuccessResponse = (params) => {
  const errorResponse = {}
  errorResponse[params.ServiceRequestId] = {
    ResponseHeader: params.ResponseHeader,
    ResponseBody: params.ResponseBody,
    Status: {
      DisplayText: params.DisplayText || '',
      ErrorCode: params.ErrorCode || '',
      ErrorMessage: params.ErrorMessage || '',
      StatusCode: '0',
    },
  }
  return errorResponse
}

module.exports = {
  constructErrorResponse,
  constructSuccessResponse,
}
