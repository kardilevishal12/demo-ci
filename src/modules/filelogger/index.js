require('winston-daily-rotate-file')
const { v1 } = require('uuid')
const fecha = require('fecha').format
const { createLogger, format, transports } = require('winston')
const { combine, timestamp, printf } = format

const myFormat = printf(({ message }) => {
  var t = ''
  for (const name of Object.keys(message)) {
    t += message[name] + '|~|'
  }
  return `|**|${t}`
})

const myCustomLevels = {
  levels: {
    fl: 0,
    con: 1,
  },
  colors: {
    fl: 'green',
    con: 'blue',
  },
}

const logger = createLogger({
  levels: myCustomLevels.levels,
  transports: [
    new transports.DailyRotateFile({
      filename: global.gConfig.logsDirectory + '/application-%DATE%.log',
      datePattern: 'YYYY-MM-DD',
      zippedArchive: true,
      maxSize: '1024m',
      maxFiles: '3650d',
      level: 'fl',
      format: combine(timestamp({ format: 'DD-MM-YYYY' }), myFormat),
    }),
  ],
})

const logs = createLogger({
  transports: [
    new transports.Console({
      level: 'info',
      format: combine(
        timestamp({ format: 'DD-MM-YYYY HH:mm:ss' }),
        format.printf(({ message, timestamp }) => {
          if (typeof message == 'object') {
            return `${timestamp} ${message.stack}`
          }
          return `${timestamp} ${process.pid} ${message}`
        })
      ),
    }),
  ],
})

function toDatetime() {
  return fecha(new Date(), 'YYYY-MM-DD HH:mm:ss.SSS')
}

module.exports = { logger, toDatetime, logs, v1, fecha }
