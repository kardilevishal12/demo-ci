const { deflate, unzip } = require('zlib')
const { promisify } = require('util')

const do_zip = promisify(deflate)
const do_unzip = promisify(unzip)

const unzipData = async (request) => {
  try {
    const buffer = Buffer.from(request, 'base64')
    const result = await do_unzip(buffer)
    return result.toString()
  } catch (error) {
    return null
  }
}

const zipData = async (data) => {
  try {
    const payload = data ? Buffer.from(data) : 'NA'
    return (await do_zip(payload)).toString('base64')
  } catch (err) {
    return null
  }
}

module.exports = { unzipData, zipData }
