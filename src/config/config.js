const dotenv = require('dotenv')
const path = require('path')

dotenv.config({
  path: path.join(__dirname, `../environment/.dev.env`),
})

const logsDirectory = process.env.logs_directory + (process.env.HOSTNAME || '')
module.exports = {
  timeout: process.env.timeout,
  configId: process.env.config_id,
  version: process.env.version,
  port: process.env.port,
  instances: process.env.instances || 1,
  logsDirectory: logsDirectory,
  ServiceName: process.env.ServiceName,
  NoOfDays: process.env.days,

  db_mssql_test: {
    enable: process.env.db_mssql_test_enable,
    name: process.env.db_mssql_test_name,
    username: process.env.db_mssql_test_username,
    password: process.env.db_mssql_test_password,
    host: process.env.db_mssql_test_host,
    port: process.env.db_mssql_test_port,
    schema: process.env.db_mssql_test_schema,
  },

  db_postgres_students: {
    enable: process.env.db_postgres_students_enable,
    name: process.env.db_postgres_students_name,
    username: process.env.db_postgres_students_username,
    password: process.env.db_postgres_students_password,
    host: process.env.db_postgres_students_host,
    port: process.env.db_postgres_students_port,
    schema: process.env.db_postgres_students_schema,
  },

  log: {
    console: {
      general_enable: process.env.log_console_general_enable,
      audit_enable: process.env.log_console_audit_enable,
      exception_enable: process.env.log_console_exception_enable,
    },
    file: {
      general_enable: process.env.log_file_general_enable,
      audit_enable: process.env.log_file_audit_enable,
      exception_enable: process.env.log_file_exception_enable,
    },
  },

  db_redis: {
    enable: process.env.db_redis_enable,
    host: process.env.db_redis_host,
    port: process.env.db_redis_port,
    encryption: process.env.db_redis_enable_encryption,
    restart_required: 'YES',
  },
}
