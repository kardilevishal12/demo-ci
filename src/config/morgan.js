const morgan = require('morgan')
const config = require('./config')

morgan.token('message', (req, res) => res.locals.errorMessage || '')

const getIpFormat = () => (config.env === 'prod' ? ':remote-addr - ' : '')
const successResponseFormat = `${getIpFormat()} Status-Code :status - Response-Time :response-time ms`
const errorResponseFormat = `${getIpFormat()} Status-Code :status - Response-Time :response-time ms - message: :message`

const successHandler = morgan(successResponseFormat, {
  skip: (req, res) => res.statusCode >= 400,
  stream: { write: (message) => console.info(message.trim()) },
})

const errorHandler = morgan(errorResponseFormat, {
  skip: (req, res) => res.statusCode < 400,
  stream: { write: (message) => console.error(message.trim()) },
})

module.exports = {
  successHandler,
  errorHandler,
}
