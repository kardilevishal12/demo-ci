const config = require('./src/config/config')
global.gConfig = config
require('tls').DEFAULT_MIN_VERSION = 'TLSv1'

require('./src/app')
